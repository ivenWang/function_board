import { keywordAnalysis } from '../../src/parser/index'
// const { keywordAnalysis } = require('../../src/parser/index')
// const keywordAnalysis = require('../../src/parser/index')
test('测试分词器', () => {
    console.log(keywordAnalysis)
    expect(keywordAnalysis(Array.from('x*cos(x)/2'))).toEqual(['x', "*", 'cos', "(", 'x', ")", '/', '2']);
    expect(keywordAnalysis(Array.from('cos(x)/x'))).toEqual(['cos','(','x',')','/','x'])
    
});