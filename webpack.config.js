const path = require('path');
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');



module.exports = {
    entry: {
        'index.js': './src/index.jsx',
    },
    devtool: 'eval-source-map',
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,     // 排除 node_modules 文件夹
                use: {
                    loader: 'babel-loader',     // babel-loader  babel-loader处理JSX语法的。
                    options: {
                        babelrc: true,
                        presets: ['@babel/preset-react', '@babel/preset-env'],
                        cacheDirectory: true
                    }
                }
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: '函数画板0.5.0',
            template: './src/index.ejs',
            publicPath: './',
            filename: 'index.html',
            chunks: ['index.js']
        }),
        new MiniCssExtractPlugin(),
    ],
    output: {
        filename: '[name]_bundle_[hash].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },
    devServer: {
        contentBase: './www',
        hot: true,
        // port: 8080
        port: 23333
    },
    stats: {
        // lets you precisely control what bundle information gets displayed
        preset: "errors-only",
        // A stats preset

        /* Advanced global settings (click to show) */

        env: true,
        // include value of --env in the output
        outputPath: true,
        // include absolute output path in the output
        publicPath: true,
        // include public path in the output

        assets: true,
        // show list of assets in output
        /* Advanced assets settings (click to show) */

        entrypoints: true,
        // show entrypoints list
        chunkGroups: true,
        // show named chunk group list
        /* Advanced chunk group settings (click to show) */

        chunks: true,
        // show list of chunks in output
        /* Advanced chunk group settings (click to show) */

        modules: true,
        // show list of modules in output
        /* Advanced module settings (click to show) */
        /* Expert module settings (click to show) */

        /* Advanced optimization settings (click to show) */

        children: true,
        // show stats for child compilations

        errors: true,
        // show errors
        errorDetails: true,
        // show details for errors
        errorStack: true,
        // show internal stack trace for errors
        moduleTrace: true,
        // show module trace for errors
        // (why was causing module referenced)

        builtAt: true,
        // show timestamp in summary
        errorsCount: true,
        // show errors count in summary
        warningsCount: true,
        // show warnings count in summary
        timings: true,
        // show build timing in summary
        version: true,
        // show webpack version in summary
        hash: true,
        // show build hash in summary
    },
};
