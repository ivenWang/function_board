import { combineReducers } from 'redux'

let data = {
    logList:[
        '欢迎使用函数画板0.7.0！技术支持请联系QQ：2653591247。'
    ],
    params:{
        a:0,
        b:0,
        c:0
    },
    functionList:[],
    nowFun:-1
};
export const addLog = (log) => {
    return {
        type: '添加日志',
        log
    }
}

export const setParam=(params)=>{
    return {
        type: '设置函数的参数表单',
        params
    }
}

export const setParamValue=(key,value)=>{
    return {
        type: '设置函数参数的值',
        key,
        value
    }
}

export const setFunctionList=(list)=>{
    return {
        type: '设置可以绘制的函数列表（既拥有的基本函数模块）',
        list
    }
}
export const setNowFunction=(id)=>{
    return {
        type: '设置当前准备画哪个函数',
        id
    }
}

export const funBoardUI = (state, action) => {
    switch (action.type) {
        case '添加日志':
            let newLog=state.logList;
            newLog.push(action.log)
            return Object.assign({}, state, {
                logList: newLog
            })
        case '设置函数的参数表单':
            return Object.assign({},state,{
                params:action.params
            })
        case '设置函数参数的值':
            let newParams=Object.assign({},state.params);
            let key=action.key;
            newParams[key]=action.value;
            
            return Object.assign({},state,{
                params:newParams
            })
        case '设置可以绘制的函数列表（既拥有的基本函数模块）':
            return Object.assign({}, state, {
                functionList: action.list
            })
        case '设置当前准备画哪个函数':
            return Object.assign({}, state,{
                nowFun:action.id
            })
        default:
            return state ? state : data;
    }
}

export default combineReducers({
    funBoardUI
})
