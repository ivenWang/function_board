import Commond from './Commond';
export default class Clear extends Commond{
    constructor(cmd){
        super(cmd);
        this.name = "命令模板";
        this.id = "Commond";
        this.version = "0.1"; //模块版本
        this.program = "0.6"; //本模块是为该版本的主程序设计
        this.help="清空控制台的文字";
    }
    run(){
        document.getElementsByTagName("textarea").item(0).innerHTML = "欢迎使用函数画板！技术支持请联系QQ：2653591247。";
    }
}