settings={
	name:"设置命令",
	id:"settings",
	version:"0.1",//模块版本
	program:"0.4",//本模块是为该版本的主程序
	commands:{
		canvas:{
			id:"canvas",
			help:"画布设定",
			run:function(par){
				var canvasInter=function(p){
					if(typeof p[0]=="string"){
						p[0].toLowerCase();
					}
					switch(p[0]){
						case "help":
							boot.system.consoleMsg("\t可用参数：");
							boot.system.consoleMsg("\tdown 【单位长度】\t\t\t设置坐标系统下偏移（此为预留功能）");
							boot.system.consoleMsg("\theight 【像素】\t\t\t设置画布宽");
							boot.system.consoleMsg("\thelp\t\t\t获取帮助");
							boot.system.consoleMsg("\tleft 【单位长度】\t\t\t设置坐标系统左偏移（此为预留功能）");
							boot.system.consoleMsg("\tright 【单位长度】\t\t\t设置坐标系统右偏移（此为预留功能）");
							boot.system.consoleMsg("\tunit 【像素】\t\t\t设置坐标系单位长度");
							boot.system.consoleMsg("\tup 【单位长度】\t\t\t设置坐标系统上偏移（此为预留功能）");
							boot.system.consoleMsg("\twidth 【像素】\t\t\t设置画布宽");
							p.shift();
						break;
						case "width":
							p.shift();
							main.canvas.width=p[0];
							p.shift();
							main.xys();
						break;
						case "height":
							p.shift();
							main.canvas.height=p[0];
							p.shift();
							main.xys();
						break;
						case "left":
							p.shift();
							main.coordinate.x=-p[0];
							p.shift();
							main.xys();
						break;
						case "right":
							p.shift();
							main.coordinate.x=p[0];
							p.shift();
							main.xys();
						break;
						case "up":
							p.shift();
							main.coordinate.x=p[0];
							p.shift();
							main.xys();
						break;
						case "down":
							p.shift();
							main.coordinate.x=-p[0];
							p.shift();
							main.xys();
						break;
						case "unit":
							p.shift();
							main.coordinate.units=p[0];
							p.shift();
							main.xys();
						break;
						default:
							boot.system.consoleMsg("非法的参数："+p[0]);
							p.shift();
					}
					if(p.length>0){
						canvasInter(p);
					}
				}
				canvasInter(par);
			}
		}
	},
	start:function(){
		cmd.cmdReg("canvas","画布设定",settings.commands.canvas.run);
	},
}