import Main from "../Main"
export default class Base {
    constructor(main) {
        this.name = "基础函数模板";
        this.id = "Linear";
        this.version = "0.5"; //模块版本
        this.program = "0.9.3"; //本模块是为该版本的主程序设计
        /**
         * @type {Main}
         */
        this.main=main;
    }
    get maxX(){
        return this.main.getXmax();
    }
    get minX(){
        return this.main.getXmin();
    }
    //模块初始化
    init() {
        throw new SyntaxError('没有覆盖init方法。init是用于函数初始化参数的方法。从0.8.0版本开始，请用init返回this.formReady以设置函数参数。')
    };
    //画函数
    draw(obj) {
        throw new SyntaxError('没有覆盖draw方法。draw是用于画函数的方法。')
    }
    formReady(a,b,c){
        let rel={}
        if(a){
            rel[a]=0;
        }
        if(b){
            rel[b]=0;
        }
        if(c){
            rel[c]=0;
        }
        return rel;
    }
    log(msg){
        this.main.log(this.name,msg);
    }
    /**
     * 将数学坐标转换成屏幕坐标
     * @param {nubmer} x 
     * @param {number} y 
     */
    coor(x,y){
        return this.main.coor(x,y)
    }
}
