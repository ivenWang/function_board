import Base from './Base';
export default class Linear extends Base {
	constructor(env){
		super(env);
		this.name= "一次函数绘制模块";
		this.id= "Linear";
		this.version= "0.5"; //模块版本
		this.program= "0.8"; //本模块是为该版本的主程序设计
	}
	
//模块初始化
	init(){
		this.log("准备就绪");
		return this.formReady("K","B");
	};
//画一次函数
	draw(obj){
		let k=new Number(obj["K"]);
		let b = new Number(obj["B"]);
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		var xy=this.main.coor(-(this.main.canvas.width/2)/this.main.coordinate.units,k*(0-(this.main.canvas.width/2)/this.main.coordinate.units)+b);
		console.log(xy)
		line.moveTo(xy['x'],xy['y']);
		let nxy=this.main.coor((this.main.canvas.width/2)/this.main.coordinate.units,k*(this.main.canvas.width/2)/this.main.coordinate.units+b);
		console.log(nxy)
		line.lineTo(nxy['x'],nxy['y']);
		line.strokeStyle ="#FF3F3F";
		line.stroke();
		this.log("函数绘制完成");
	}
}
