import Base from './Base';
export default class Quadratic extends Base {
	constructor(env){
		super(env);
		this.name= "二次函数绘制模块";
		this.id= "quadratic";
		this.version ="0.4";//模块版本
		this.program= "0.7";//本模块是为该版本的主程序设计
	}

//模块初始化
	init(){
		this.log("准备就绪");
		return this.formReady("a","b","c");
	};
//画一次函数
	draw(obj){
		let a = new Number(obj['a']);
		let b = new Number(obj['b']);
		let c = new Number(obj['c']);
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		var xy;
		xy=this.main.coor(this.main.getXmin(),a*this.main.getXmax()+b*this.main.getXmin()+c);
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		for(let i=this.main.getXmin();i<=this.main.getXmax();i=i+this.main.getMinUnit()){		
				xy=this.main.coor(i,a*i*i+b*i+c);
				line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
				line.strokeStyle="#FF3F3F3F3F";
		}
		line.strokeStyle="#FF3F3F3F3F";
		line.stroke();
		this.log("函数绘制完成");
	}
}

