import Base from './Base';
export default class Sin extends Base{
	constructor(main){
		super(main);
		this.name = "x sin x";
		this.id = "XMSinX";
		this.version = "0.5";//模块版本
		this.program = "0.10.0";//本模块是为该版本的主程序设计
	}

//模块初始化
	init(){
		this.log("准备就绪");
		return this.formReady("A","ω","φ");
	};
//画一次函数
	draw(obj){
		let a = new Number(obj["A"]);
		let w = new Number(obj["ω"]);
		let p = new Number(obj["φ"]);
		this.main.draw(function (x) {
			return x * a*Math.sin(w*x+p)
		})
		this.log("函数绘制完成");
	}
}

