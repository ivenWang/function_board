import Base from './Base';
export default class Exp extends Base{
	constructor(main){
		super(main)
		this.name= "指数函数绘制模块";
		this.id= "Power";
		this.version= "0.4";//模块版本
		this.program= "0.9";//本模块是为该版本的主程序设计
	}
	init(){
		return this.formReady("a");
	}
	draw(obj){
		let a = new Number(obj['a']);
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		let xy=this.main.coor(this.main.getXmin(),Math.pow(a,this.main.getXmin()));
		console.log("x:this.main.getXmin() y:"+Math.pow(a,this.main.getXmax()));
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		for(let i=this.main.getXmin();i<=this.main.getXmax();i=i+this.main.getMinUnit()){		
			xy=this.main.coor(i,Math.pow(a,i));
			console.log("x:"+i+"y:"+Math.pow(a,i));
			line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
			line.strokeStyle="#FF3F3F";
		}
		line.strokeStyle="#FF3F3F";
		line.stroke();
	}
}
