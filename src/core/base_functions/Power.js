import Base from './Base';
export default class Power extends Base{
	constructor(t){
		super(t)
		this.name= "幂函数绘制模块";
		this.id= "Power";
		this.version= "0.5";//模块版本
		this.program= "0.9.5";//本模块是为该版本的主程序设计
	}
	init(){
		return this.formReady("a");
	}
	draw(obj){
		let a = new Number(obj['a']);
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		var xy;
		xy=this.main.coor(this.main.getXmin(),Math.pow(this.main.getXmin(),a));
		console.log("x:this.main.getXmin() y:"+Math.pow(this.main.getXmin(),a));
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		this.main.xPointArray.forEach((i)=> {
			xy=this.main.coor(i,Math.pow(i,a));
			line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
			line.strokeStyle="#FF3F3F";
		})
		line.strokeStyle="#FF3F3F";
		line.stroke();
	}
}


