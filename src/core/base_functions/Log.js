import Base from './Base';
export default class Log extends Base{
	constructor(main){
		super(main);
		this.name= "对数函数绘制模块";
		this.id= "Power";
		this.version= "0.4";//模块版本
		this.program= "0.9.1";//本模块是为该版本的主程序设计
	}
	init(){
		return this.formReady("a");
	}
	draw(o){
		let a=new Number(o['a']);
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		let xy=this.main.coor(this.main.getMinUnit(),Math.log(this.main.getMinUnit())/Math.log(a));
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		console.log("x:"+Math.round(xy['x'])+" y:"+Math.round(xy['y']));
		for(let i=this.main.getMinUnit();i<=this.main.getXmax();i=i+this.main.getMinUnit()){		
			xy=this.main.coor(i,Math.log(i)/Math.log(a));
			line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
			console.log("x:"+Math.round(xy['x'])+" y:"+Math.round(xy['y']));
			line.strokeStyle="#FF3F3F";
			line.strokeStyle="#FF3F3F";
			line.stroke();
		}
	}
}



