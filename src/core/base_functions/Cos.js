import Base from './Base';
export default class Cos extends Base{
	constructor(m){
		super(m);
		this.name = "余弦函数绘制模块";
		this.id = "Cos";
		this.version = "0.4";//模块版本
		this.program = "0.9.3";//本模块是为该版本的主程序设计
	}

//模块初始化
	init(){
		this.formReady();
		this.log("准备就绪");
	};
//画一次函数
	draw(){
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		var xy;
		xy=this.coor(this.minX,Math.cos(this.minX));
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		console.log("x:"+Math.round(xy['x'])+" y:"+Math.round(xy['y']));
		for (let i = this.minX; i <= this.maxX; i = i +this.main.getMinUnit()){		
			xy=this.coor(i,Math.cos(i));
			line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
			console.log("x:"+Math.round(xy['x'])+" y:"+Math.round(xy['y']));
			line.strokeStyle="#FF3F3F";
			line.stroke();
		}
		// boot.system.consoleMsg(Cos.name+"信息：函数绘制完成");
	}
}

