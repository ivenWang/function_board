import Base from './Base';
export default class Rec extends Base{
	constructor(env){
		super(env)
		this.name="反比例函数绘制模块";
		this.id="rec";
		this.version="0.5";//模块版本
		this.program="0.8";//本模块是为该版本的主程序设计
	};
	//模块初始化
	init(){
		this.log("准备就绪");
		return this.formReady("K");
	};
	draw(obj){
		let k = new Number(obj["K"]);
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		var xy;
		xy=this.main.coor(this.main.getXmin(),k/this.main.getXmin());
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		for(let i=this.main.getXmin();i<0;i=i+this.main.getMinUnit()){		
				xy=this.main.coor(i,k/i);
				line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
				line.strokeStyle="#FF3F3F3F3F";
		}
		xy=this.main.coor(this.main.getMinUnit(),k/this.main.getMinUnit());
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		for(let i=this.main.getMinUnit();i<=this.main.getXmax();i=i+this.main.getMinUnit()){		
				xy=this.main.coor(i,k/i);
				line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
				line.strokeStyle="#FF3F3F3F3F";
		}
		line.strokeStyle="#FF3F3F3F3F";
		line.stroke();
}
}





