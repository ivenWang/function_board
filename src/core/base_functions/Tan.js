import Base from './Base';
export default class Tan extends Base{
	constructor(m){
		super(m);
		this.name = "正切函数绘制模块";
		this.id = "Tan";
		this.version = "0.5";//模块版本
		this.program = "0.11.0";//本模块是为该版本的主程序设计
	}

//模块初始化
	init(){
		
		this.log("准备就绪");
		return this.formReady();
	}
//画一次函数
	draw(){
		var line=document.getElementById("xy").getContext("2d");
		line.beginPath();
		var xy;
		xy=this.coor(this.minX,Math.tan(this.minX));
		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		console.log("x="+Math.round(xy['x'])+" y="+Math.round(xy['y']));
		this.main.draw(Math.tan)
		// for(let i=this.minX;i<=this.maxX;i=i+this.main.getMinUnit()){	
		// 	if(Math.tan(i-this.main.getMinUnit())<Math.tan(i)){	
		// 		xy=this.coor(i,Math.tan(i));
		// 		line.lineTo(Math.round(xy['x']),Math.round(xy['y']));
		// 		console.log("x="+Math.round(xy['x'])+" y="+Math.round(xy['y']));		
		// 	}else{
		// 		xy=this.coor(i,Math.tan(i));
		// 		line.moveTo(Math.round(xy['x']),Math.round(xy['y']));
		// 		console.log("x="+Math.round(xy['x'])+" y="+Math.round(xy['y']));	
		// 	}
		// line.strokeStyle="#FF3F3F";
		// line.stroke();
		// }
	this.log("函数绘制完成");
	}
}