import Base from './Base';
export default class E extends Base{
	constructor(main){
		super(main);
		this.name = "E^x";
		this.id = "E";
		this.version = "0.1";//模块版本
		this.program = "0.10.1";//本模块是为该版本的主程序设计
	}
	init(){
		this.log("准备就绪");
		return this.formReady();
	};
	draw(){
		this.main.draw(function (x) {
			return Math.pow(Math.E,x);
		})
		this.log("函数绘制完成");
	}
}

