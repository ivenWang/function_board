import React from 'react';
import { connect } from 'react-redux';

import ParamInput from '../param-input/ParamInput.jsx';
import {setParamValue} from '../../reducers/index';
class ParamAreaBase extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        let keys = this.props.params?Object.keys(this.props.params):[]
        let pList = keys.map((key) =>{
            // let k=Object.keys(p)[0]
            return (<ParamInput keyName={key} onInputParams={(k,v)=>{
                this.props.onInputParams(k,v);
            }}/>);
        })
        return(<div>
            {pList}
        </div>)
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onInputParams: (k,y) => {
            dispatch(setParamValue(k,y))
        }
    }
}

const ParamArea = connect(
    null,
    mapDispatchToProps
)(ParamAreaBase)
export default ParamArea;