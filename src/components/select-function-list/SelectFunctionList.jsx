import React from 'react';
import { connect } from 'react-redux';

import {Base} from '../../core/index'; //仅用于代码提示 勿调用
import { setParam, setNowFunction} from '../../reducers/index';
class SelectFunctionListBase extends React.Component{
    constructor(props){
        super(props);
        this.defaultProps = {
            functionList: []
        }
    }
    render(){
        /**
         * @type {Array<Base>}
         */
        let list = this.props.functionList;
        console.log(list);
        window.testList=list;
        console.error(list);
        let domList = list.map((funObj,i)=>{
            console.log(funObj,i)
            return (
                <option 
                    id={funObj.constructor.name}
                    value={i}
                >{funObj.name}</option>
            )
        })
        return (<select onChange={(e)=>{
            let fun = this.props.functionList[e.target.value]
            if (fun&&fun.init){
                let params = fun.init();
                console.error(params)
                this.props.onInputParams(params);
            }
            this.props.onSetNowFun(e.target.value);
        }}>
            <option value="-1">请选择：</option>
            {domList}
        </select>)
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onInputParams: (obj) => {
            dispatch(setParam(obj));
        },
        onSetNowFun:(id)=>{
            dispatch(setNowFunction(id))
        }
    }
}

const SelectFunctionList = connect(
    null,
    mapDispatchToProps
)(SelectFunctionListBase)
export default SelectFunctionList;