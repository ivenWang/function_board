import React from 'react';
class ParamInput extends React.Component{
    constructor(props){
        super(props);
        this.defaultProps={
            onInputParams:()=>{}
        }
    }
    render(){
        let key = this.props.keyName;
        // console.error(key)
        // let key = Object.keys(obj)[0]
        return (<div>
            <span id={key}>{key}</span>
            <input type="number" id={`n${key}`} onChange={(e)=>{
                this.props.onInputParams(key, Number(e.target.value))
            }} />
        </div>)
    }
}
export default ParamInput;