import React from 'react';
import { connect } from 'react-redux';
import { setFunctionList} from '../../reducers/index';
import Main from '../../core/index';
import LogArea from '../log-area/LogArea.jsx';
import ParamArea from '../param-area/ParamArea.jsx';
import SelectFunctionList from '../select-function-list/SelectFunctionList.jsx';
import parser from '../../parser/index.js';
import {Input, Button } from 'antd'
class AppBase extends React.Component{
    constructor(props){
        super(props);
        this.main = new Main();
        this.state = {
            userDefinedFunctionStr: ''
        }
        // this.main.init();
        // this.state={
        //     functionList: this.main.functionList
        // };
        this.userDefinedChange = (e)=> {
            console.log(e.target.value, this)
            this.setState({
                userDefinedFunctionStr: e.target.value
            })
            console.log(this.state.userDefinedFunctionStr)
        }
        this.userDefindFun=() => {
            const f = parser(this.state.userDefinedFunctionStr)
            this.main.draw(f);
            console.log('user defind function')
        }
    }
    componentDidMount(){
        this.main.init();
        console.log('fuck',this.main.functionList)
        this.props.loadBaseFunctionModel(this.main.functionList);
        // this.updateFunctionlist();       
    }
    
    // updateFunctionlist(){
    //     console.error('fuck', this.main.functionList)
    //     this.setState({
    //         functionList: this.main.functionList
    //     })
    // }
    render(){
        return (<React.Fragment>
            <canvas width="1001" height="1001" id="xy" >你的浏览器或操作系统版本过低，请升级浏览器或操作系统</canvas>
            <br /><button id="init" onClick={()=>{
                this.main.init();
            }}>初始化</button><br />
控制台消息
            <br />
            <LogArea />
            <br />
命令行
            <input type="text" id="cmd" width="500" onKeyPress=""/><button id="run">执行</button>
            <Input 
                type="text"
                value={this.state.userDefinedFunctionStr}
                onChange={this.userDefinedChange}
                placeholder='自己写个函数（测试中，仅支持JS语法，如x*Math.cos(x)）'
            />
            <Button onClick={this.userDefindFun}>画自定义函数</Button>
                <br />
常用函数
            <SelectFunctionList functionList={this.props.functionList}></SelectFunctionList>
            <ParamArea params={this.props.params}></ParamArea>
            <button id="draw" onClick={()=>{
                let funId = this.props.nowFun;
                if (this.props.functionList[funId]){
                    this.props.functionList[funId].draw(this.props.params);
                }else{
                    throw new ReferenceError(`id为${funId}的函数不存在！！！`);
                }
                
                // console.error(this.props.params, this.props.nowFun);
            }}>绘制</button>
                    <div id="scriptspan">
                    </div>
        </React.Fragment>)
    }
}

const mapStateToProps = (state) => {
    return {
        params: state.funBoardUI.params,
        functionList: state.funBoardUI.functionList,
        nowFun: state.funBoardUI.nowFun
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loadBaseFunctionModel: funList => {
            dispatch(setFunctionList(funList))
        }
    }
}

let App = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppBase);

export default App;