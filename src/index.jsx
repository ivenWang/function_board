import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import funBoard from './reducers/index'
import App from './components/app/App.jsx';

import './css/main.css';

let store = createStore(funBoard)
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);