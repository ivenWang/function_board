# 函数画板

#### 介绍
我实在不知道干嘛了，实在不行修改小时候的作品吧。

演示地址（gitee page） [函数画板](http://ivenwang.gitee.io/function_board/)

演示地址（我自己的网站）：[函数画板](http://fx.tools.largeq.cn)

#### 软件架构 & 发行说明
软件架构说明
我小时候，为了追求高大上，加了很多多余的东西。本项目的第一个目标是把这些多余的东西给简化。
所以短期内master分支不能够保证正常运行，但这个项目估计不会有除了我以外的其它人看。我已经把上一个版本（2015年的版本）创建了一个发行版本。

在我长大后的重构版本中，我决定采用react来做UI，但核心逻辑变化并不算大。在下个发行版本中，不会有任何功能上的更新，甚至功能还会减少（比如控制台与日志功能在下一个发行版本中不会恢复。）。但是界面会有变化。

#### 安装教程

1.  先安装依赖 `npm install`
2.  如果调试，运行`npm run start`
3.  如果构建，运行`npm run build`

#### 使用说明

1.  npm start 会在[localhost:23333](http://localhost:23333)启动
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
